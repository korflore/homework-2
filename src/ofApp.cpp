#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //Set background color to gray
    ofBackground(51);
    
    //places our spheres in the x, y, z direction.
    for(int x = 0; x < xSpheres; x++){
        for(int y = 0; y < ySpheres; y++){
            for(int z = 0; z < zSpheres; z++){
                sphere[x][y][z].setPosition((x * 40)-1000, (y * 40)-400, (z*40)-400);
            }
            
        }
    }
    
    //set camera distance and lighting
    cam.setDistance(1800);
    ofSetSmoothLighting(true);
    light1.setDiffuseColor(ofFloatColor(.95, .95, .65));
    light1.setSpecularColor(ofFloatColor(1.f, 1.f, 1.f));
    light1.setPosition(-300, 200, 1000);
    
}

//--------------------------------------------------------------
void ofApp::update(){
    //creates ofnoise and sets the x and y values of each sphere to it
    float myTime = ofGetSystemTimeMillis();
    for (int i = 0; i < xSpheres; i++) {
        for(int j = 0; j < ySpheres; j++){
            for(int k = 0; k < zSpheres; k++){
                glm::vec3 pos = sphere[i][j][k].getPosition();
                float myNoise = 60.0 * ofSignedNoise(glm::vec4(pos/400.0, myTime/1000.0));
                if (myNoise < 0) myNoise = 3;
                sphere[i][j][k].set(myNoise,myNoise);
            }
            
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    cam.begin();
    ofEnableDepthTest();
    ofEnableLighting();
    light1.enable();
    
    //rotate the flag 30 degrees
    ofRotateDeg(30, 0, 1, 0);
    
    //color and draw each sphere onto the screen
    for(int x = 0; x < xSpheres; x++){
        for(int y = 0; y < ySpheres; y++){
            for(int z = 0; z < zSpheres; z++){
                
                //stars section
                if(x <= starsX && y > starsY){
                    if(x <= starsX && y >starsY ){
                        ofSetColor(0, 0, 255);
                        if(x%3 ==0 && x != 0 && y != 0 && y != starsY){
                            ofSetColor(255, 255, 255);
                        }
                        sphere[x][y][z].draw();
                        
                    }
                    //white stripes section next to stars
                    if(x > starsX && y%2 == 0){
                        ofSetColor(255, 255, 255);
                        sphere[x][y][z].draw();
                    }
                    //red stripes section next to stars
                    if(x>starsX && y%2 != 0){
                        ofSetColor(255, 0, 0);
                        sphere[x][y][z].draw();
                    }
                    //other stripes past the stars
                }else{
                    if( y%2 == 0){
                        ofSetColor(255, 255, 255);
                        sphere[x][y][z].draw();
                    }
                    
                    if( y%2 != 0){
                        ofSetColor(255, 0, 0);
                        sphere[x][y][z].draw();
                    }
                }
            }
            
        }
    }
    
    ofDisableLighting();
    ofDisableDepthTest();
    
    cam.end();
    
    cam.draw();
}

