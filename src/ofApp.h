#pragma once

#include "ofMain.h"

#define xSpheres 50
#define ySpheres 26
#define zSpheres 5
#define starsX 21
#define starsY 14

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
    

    ofEasyCam cam;
    ofLight light1;
    ofMaterial mat;
    
    ofSpherePrimitive sphere[xSpheres][ySpheres][zSpheres];
    ofColor sphereColors[xSpheres*ySpheres];
		
};
